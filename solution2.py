# Soulution question 2
def solution(A):
    deepest = 0
    pit = lambda p, q, r: min(A[p] - A[q], A[r] - A[q])
    p = 0
    r = -1
    q = -1
    l = len(A)
    for i in xrange(0, l):
        if q<0 and A[i]>=A[i-1]:
            q = i -1
        if (q>=0 and r<0) and (A[i]<=A[i-1] or i+1==l):
            r = i-1
            deepest = max(deepest, pit(p, q, r))
            p = i-1
            q = -1 
            r = -1
    return deepest if deepest else -1